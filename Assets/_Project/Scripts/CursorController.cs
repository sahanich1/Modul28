﻿using UnityEngine;

public class CursorController
{
    private bool isWaitingForChooseDialogueVariant;

    public void Init(DialogueController dialogueController)
    {
        dialogueController.SpeechStarted += OnSpeechStarted;
        dialogueController.WaitingForChooseVariant += OnWaitingForChooseVariant;
    }

    public void SetMode(GameMode gameMode)
    {
        if (gameMode == GameMode.Menu 
            || gameMode == GameMode.Dialogue && isWaitingForChooseDialogueVariant)
        {
            ShowCursor();
        }
        else 
        {
            HideCursor();
        }
    }

    private void OnWaitingForChooseVariant(DialogueSpeech _)
    {
        ShowCursor();
        isWaitingForChooseDialogueVariant = true;
    }

    private void OnSpeechStarted(DialogueSpeech _)
    {
        HideCursor();
        isWaitingForChooseDialogueVariant = false;
    }

    private void ShowCursor()
    {
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
    }

    private void HideCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
}
