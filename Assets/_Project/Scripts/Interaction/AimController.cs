using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimController : MonoBehaviour
{
    [SerializeField]
    private float maxTargetDistance = 10;
    [SerializeField]
    private LayerMask aimObjectsLayerMask;

    private AimDisplay aimDisplay;
    private Actor player;
    private FreeLookCameraController freeLookCameraController;

    public Actor Target { get; private set; }

    public void Init(FreeLookCameraController freeLookCameraController, AimDisplay aimDisplay, Actor player)
    {
        this.player = player;
        this.aimDisplay = aimDisplay;
        this.freeLookCameraController = freeLookCameraController;
    }

    public void ResetTarget()
    {
        SetTarget(null);
    }

    public void RefreshTarget()
    {
        Actor target = null;
        if (Vector3.Dot(freeLookCameraController.MainCamera.transform.forward, player.transform.forward) <= 0)
        {
            SetTarget(target);
            return;
        }

        Ray ray = new Ray(player.GetAimingStartPoint(), freeLookCameraController.MainCamera.transform.forward);

        if (Physics.SphereCast(ray, player.GetAimingRadius(), out var hitInfo, maxTargetDistance, aimObjectsLayerMask))
        {
            Actor actor = hitInfo.collider.GetComponent<Actor>();
            target = actor != null && actor.IsInteractive && hitInfo.distance <= actor.MaxDistanceForInteraction ? actor : null;
        }

        SetTarget(target);
    }

    private void SetTarget(Actor target)
    {
        Target = target;
        aimDisplay.Refresh(Target);
    }
}
