﻿using UnityEngine;

[RequireComponent(typeof(Collider))]
public class ActorInteractionTrigger : MonoBehaviour
{
    [field: SerializeField]
    public Actor Actor { get; private set; }

    public void Destroy()
    {
        Destroy(gameObject);
    }
}
