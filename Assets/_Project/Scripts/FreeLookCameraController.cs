using Cinemachine;
using UnityEngine;


[RequireComponent(typeof(CinemachineFreeLook))]
public class FreeLookCameraController : MonoBehaviour, AxisState.IInputAxisProvider
{
    [SerializeField]
    private string HorizontalInput = "Mouse X";
    [SerializeField]
    private string VerticalInput = "Mouse Y";

    public CinemachineFreeLook FreeLookCam { get; private set; }
    public Camera MainCamera { get; private set; }
    private bool isControlActive = true;

    private void Awake()
    {
        FreeLookCam = GetComponent<CinemachineFreeLook>();
        MainCamera = Camera.main;
    }

    float AxisState.IInputAxisProvider.GetAxisValue(int axis)
    {
        if (!isControlActive)
            return 0;

        switch (axis)
        {
            case 0: return Input.GetAxis(HorizontalInput);
            case 1: return Input.GetAxis(VerticalInput);
            default: return 0;
        }
    }

    public void ResetPosition()
    {
        Vector3 lookAtBackPosition = FreeLookCam.Follow.position + FreeLookCam.m_Orbits[1].m_Height * Vector3.up
            - FreeLookCam.m_Orbits[1].m_Radius * FreeLookCam.LookAt.forward;
        FreeLookCam.ForceCameraPosition(lookAtBackPosition, Quaternion.identity);
    }

    public void SetControlActive(bool active)
    {
        isControlActive = active;
    }
}
