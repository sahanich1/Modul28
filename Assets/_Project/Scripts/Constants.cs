using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants
{
    public static class Animation
    {
        public static class State
        {
            public const string Idle = "Idle";
            public const string IdleUpperBody = "Upper Body Idle";
            public const string IdleSpeech = "Speech Idle";
        }
        public static class Parameter
        {
            public const string DialogueModeBool = "DialogueMode";
            public const string SpeechPlayingBool = "SpeechPlaying";
            public const string DialogueModeEnterTrigger = "EnterDialogueMode";
            public const string DialoguePlayRandomTrigger = "PlayRandomDialogueAnim";
            public const string DialogueStateID = "DialogueStateID";
                         
            public const string IdlePlayRandomTrigger = "PlayRandomIdleAnim";
            public const string IdleBlendXFloat = "IdleBlendX";
            public const string IdleBlendYFloat = "IdleBlendY";
            public const string IdleStateID = "IdleStateID";
                         
            public const string UpperBodyPlayRandomTrigger = "PlayRandomUpperBodyAnim";
            public const string UpperBodyStateID = "UpperBodyStateID";

            public const string RunningBool = "isRunning";
            public const string WalkingBool = "isWalking";
            public const string StartWalkingTrigger = "StartWalking";
            public const string ForwardFloat = "Forward";
            public const string TurnFloat = "Turn";
        }
        public static class Layer
        {
            public const string Base = "Base Layer";
            public const string UpperBody = "Upper Body Layer";
        }
    }
}
