﻿public class KeyPressController
{
    private MenuKeyListener _menuKeyListener;
    private GameplayKeyListener _gameplayKeyListener;
    private DialogueKeyListener _dialogueKeyListener;
    private CutsceneKeyListener _cutsceneKeyListener;

    private KeyListener _currentKeyListener;

    public void Init(UIEventMediator uiEventMediator, UIScreenController uiScreenController)
    {
        _menuKeyListener = new MenuKeyListener();
        _gameplayKeyListener = new GameplayKeyListener();
        _cutsceneKeyListener = new CutsceneKeyListener();
        _dialogueKeyListener = new DialogueKeyListener();

        _menuKeyListener.Init(uiEventMediator, uiScreenController);
        _gameplayKeyListener.Init(uiEventMediator);
        _dialogueKeyListener.Init(uiEventMediator);
        _cutsceneKeyListener.Init(uiEventMediator);
    }

    public void DoUpdate()
    {
        _currentKeyListener?.CatchKey();
    }

    public void SetMode(GameMode gameMode)
    {
        switch (gameMode)
        {
            case GameMode.Gameplay:
                _currentKeyListener = _gameplayKeyListener;
                break;
            case GameMode.Dialogue:
                _currentKeyListener = _dialogueKeyListener;
                break;
            case GameMode.Cutscene:
                _currentKeyListener = _cutsceneKeyListener;
                break;
            case GameMode.Menu:
                _currentKeyListener = _menuKeyListener;
                break;
            default:
                _currentKeyListener = null;
                break;
        }
    }
}