﻿using System;
using UnityEngine;

public class UIEventMediator
{
    public event Action QuitRequested;
    public event Action PauseGameRequested;
    public event Action ResumeGameRequested;
    public event Action ReturnToPreviousScreenRequested;
    public event Action<UIScreen> ShowChildScreenRequested;
    public event Action<string> ApplyToAudioMixerRequested;
    public event Action<Actor> DialogueStartRequested;
    public event Action SkipSpeechRequested;
    public event Action<Vector2, bool> PlayerMoveRequested;
    public event Action PlayerInteractionRequested;
    public event Action<int> PlayerAnswerRequested;

    internal void RequestPlayerMove(Vector2 input, bool isRunning)
    {
        PlayerMoveRequested?.Invoke(input, isRunning);
    }

    internal void RequestPlayerInteraction()
    {
        PlayerInteractionRequested?.Invoke();
    }

    internal void RequestDialogueStart(Actor actor)
    {
        DialogueStartRequested?.Invoke(actor);
    }

    internal void RequestPlayerAnswer(int answerIndex)
    {
        PlayerAnswerRequested?.Invoke(answerIndex);
    }

    internal void RequestSkipSpeech()
    {
        SkipSpeechRequested?.Invoke();
    }

    internal void RequestShowChildScreen(UIScreen childScreen)
    {
        ShowChildScreenRequested?.Invoke(childScreen);
    }

    internal void RequestQuit()
    {
        QuitRequested?.Invoke();
    }

    internal void RequestPauseGame()
    {
        PauseGameRequested?.Invoke();
    }

    internal void RequestResumeGame()
    {
        ResumeGameRequested?.Invoke();
    }

    internal void RequestReturnToPreviousScreen()
    {
        ReturnToPreviousScreenRequested?.Invoke();
    }

    internal void RequestApplyToAudioMixer(string paramName)
    {
        ApplyToAudioMixerRequested?.Invoke(paramName);
    }
}
