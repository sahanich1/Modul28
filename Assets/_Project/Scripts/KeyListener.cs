using UnityEngine;

public abstract class KeyListener
{
    public abstract void CatchKey();
}

public class MenuKeyListener : KeyListener
{
    private UIEventMediator _uiEventMediator;
    private UIScreenController _uiScreenController;

    public void Init(UIEventMediator uiEventMediator, UIScreenController uiScreenController)
    {
        _uiEventMediator = uiEventMediator;
        _uiScreenController = uiScreenController;
    }

    public override void CatchKey()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            UIScreen currentScreen = _uiScreenController.CurrentScreen;
            if (currentScreen != null && currentScreen.ParentScreen != null)
            {
                _uiEventMediator.RequestReturnToPreviousScreen();
            }
            else
            {
                _uiEventMediator.RequestResumeGame();
            }
        }
    }
}

public class DialogueKeyListener : KeyListener
{
    private UIEventMediator _uiEventMediator;

    public void Init(UIEventMediator uiEventMediator)
    {
        _uiEventMediator = uiEventMediator;
    }

    public override void CatchKey()
    {
        for (int i = 1; i <= 9; i++)
        {
            if (Input.GetKeyDown(i.ToString()))
            {
                _uiEventMediator.RequestPlayerAnswer(i - 1);
                return;
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            _uiEventMediator.RequestSkipSpeech();
        }
    }
}

public class GameplayKeyListener : KeyListener
{
    private const KeyCode runningKey = KeyCode.LeftShift;
    private const KeyCode interractionKey = KeyCode.E;

    private UIEventMediator _uiEventMediator;

    public void Init(UIEventMediator uiEventMediator)
    {
        _uiEventMediator = uiEventMediator;
    }

    public override void CatchKey()
    {
        bool isRunning = Input.GetKey(runningKey);
        _uiEventMediator.RequestPlayerMove(new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")),
            isRunning);

        if (Input.GetKeyDown(interractionKey))
        {
            _uiEventMediator.RequestPlayerInteraction();
            return;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            _uiEventMediator.RequestPauseGame();
        }
    }
}

public class CutsceneKeyListener : KeyListener
{
    private UIEventMediator _uiEventMediator;

    public void Init(UIEventMediator uiEventMediator)
    {
        _uiEventMediator = uiEventMediator;
    }

    public override void CatchKey()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            _uiEventMediator.RequestPauseGame();
        }
    }
}
