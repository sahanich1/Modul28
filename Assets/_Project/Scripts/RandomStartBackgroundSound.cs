﻿using UnityEngine;

public class RandomStartBackgroundSound : MonoBehaviour
{
    [SerializeField]
    private AudioSource audioSource;
    [SerializeField]
    private float minTimePosition;
    [SerializeField]
    private float maxTimePosition;

    private void Awake()
    {
        audioSource.Stop();
        audioSource.Play();
        audioSource.time = Random.Range(minTimePosition, maxTimePosition);
    }
}
