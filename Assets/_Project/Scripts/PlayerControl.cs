using System;
using UnityEngine;

public class PlayerControl
{
    private Actor player;
    private FreeLookCameraController cameraController;
    private UIEventMediator uiEventMediator;
    private AimController aimController;

    public void Init(UIEventMediator uiEventMediator, AimController aimController, 
        FreeLookCameraController cameraController, Actor player)
    {
        this.uiEventMediator = uiEventMediator;
        this.aimController = aimController;
        this.cameraController = cameraController;
        this.player = player;
        
        player.TriggerEntered += OnPlayerTriggerEntered;
        uiEventMediator.PlayerInteractionRequested += OnPlayerInteractionRequested;
        uiEventMediator.PlayerMoveRequested += OnPlayerMoveRequested;
    }

    private void OnPlayerMoveRequested(Vector2 input, bool isRunning)
    {
        Vector3 camForward = Vector3.Scale(cameraController.MainCamera.transform.forward, new Vector3(1, 0, 1)).normalized;
        Vector3 moveVector = input.y * camForward + input.x * cameraController.MainCamera.transform.right;
        player.Move(moveVector, isRunning);
    }

    private void OnPlayerInteractionRequested()
    {
        if (aimController.Target != null)
        {
            uiEventMediator.RequestDialogueStart(aimController.Target);
        }
    }

    private void OnPlayerTriggerEntered(Collider collider)
    {
        if (collider.TryGetComponent(out ActorInteractionTrigger actorInteractionTrigger))
        {
            uiEventMediator.RequestDialogueStart(actorInteractionTrigger.Actor);
            actorInteractionTrigger.Destroy();
        }
    }
}
