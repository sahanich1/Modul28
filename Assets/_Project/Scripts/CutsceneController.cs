﻿using System;
using UnityEngine;
using UnityEngine.Playables;

public class CutsceneController : MonoBehaviour
{
    [SerializeField]
    private bool playStartCutscene;
    [SerializeField]
    private PlayableDirector startGameTimeline;
    [SerializeField]
    private PlayableDirector endGameTimeline;

    public event Action StartCutsceneEnded;
    public event Action EndCutsceneEnded;

    public void PlayStartGameCutscene()
    {
        if (playStartCutscene)
        {
            startGameTimeline.Play();
            startGameTimeline.stopped += OnTimelineStopped;
        }
        else
        {
            StartCutsceneEnded?.Invoke();
        }

    }

    public void PlayEndGameCutscene()
    {
        endGameTimeline.Play();
        endGameTimeline.stopped += OnTimelineStopped;
    }

    private void OnTimelineStopped(PlayableDirector playableDirector)
    {
        playableDirector.stopped -= OnTimelineStopped;

        if (playableDirector == startGameTimeline)
        {
            StartCutsceneEnded?.Invoke();
        }
        else if (playableDirector == endGameTimeline)
        {
            EndCutsceneEnded?.Invoke();
        }        
    }
}
