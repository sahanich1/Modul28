using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleAnimationBehaviour : NPCAnimationBehaviour
{
    [SerializeField]
    private float minIdleBlendParam = -10;
    [SerializeField]
    private float maxIdleBlendParam = 10;

    protected override string GetIdleStateName()
    {
        return Constants.Animation.State.Idle;
    }

    protected override void GenerateAnimatorParameters(Animator animator)
    {
        if (animator == null)
        {
            return;
        }

        string idleBlendXParam = Constants.Animation.Parameter.IdleBlendXFloat;
        string idleBlendYParam = Constants.Animation.Parameter.IdleBlendYFloat;

        animator.SetFloat(idleBlendXParam, GenerateRandomParam(minIdleBlendParam, maxIdleBlendParam));
        animator.SetFloat(idleBlendYParam, GenerateRandomParam(minIdleBlendParam, maxIdleBlendParam));

        animator.SetTrigger(Constants.Animation.Parameter.IdlePlayRandomTrigger);
    }

    private float GenerateRandomParam(float min, float max)
    {
        return (float)(min + rand.NextDouble() * (max - min));
    }
}
