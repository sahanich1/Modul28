using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System.Threading.Tasks;

public abstract class NPCAnimationBehaviour : StateMachineBehaviour
{
    [SerializeField]
    private int minTimerSeconds = 0;
    [SerializeField]
    private int maxTimerSeconds = 0;

    private bool isParamGenerationTimerRunning = false;

    protected static readonly System.Random rand = new System.Random();

    protected abstract void GenerateAnimatorParameters(Animator animator);
    protected abstract string GetIdleStateName();

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (stateInfo.IsName(GetIdleStateName()) && !isParamGenerationTimerRunning)
        {
            InitializeChangeAnimatorsParamAfterTime(animator);
        }
    }

    private async void InitializeChangeAnimatorsParamAfterTime(Animator animator)
    {
        isParamGenerationTimerRunning = true;

        while (true)
        {
            // Setting random delay for next idle animation to prevent synchronous animations playback
            // of different characters
            await Task.Run(() => Thread.Sleep(rand.Next(minTimerSeconds * 1000, maxTimerSeconds * 1000)));
            if (animator == null || !animator.isActiveAndEnabled
                || !animator.GetBool(Constants.Animation.Parameter.DialogueModeBool)
                && !animator.GetBool(Constants.Animation.Parameter.WalkingBool))
            {
                break;
            }
        }

        // The animator object could have been destroyed while waiting
        if (animator != null && animator.isActiveAndEnabled)
        {
            GenerateAnimatorParameters(animator);
        }
        isParamGenerationTimerRunning = false;
    }

}
