using UnityEngine;

public class DialogueAnimationBehaviour : NPCAnimationBehaviour
{
    protected override string GetIdleStateName()
    {
        return Constants.Animation.State.IdleSpeech;
    }

    protected override void GenerateAnimatorParameters(Animator animator)
    {
        if (animator != null && animator.GetBool(Constants.Animation.Parameter.DialogueModeBool))
        {
            animator.SetTrigger(Constants.Animation.Parameter.DialoguePlayRandomTrigger);
        }
    }

}
