using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAnimationBehaviour : StateMachineBehaviour
{
    [SerializeField]
    private byte stateCount = 0;
    [SerializeField]
    private string stateIDParameterName = "AnimID";

    private readonly System.Random rand = new System.Random();

    override public void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
    {
        animator.SetInteger(stateIDParameterName, rand.Next(0, stateCount));
    }
}
