﻿using UnityEngine;
using Cinemachine;

public class AnimatedPlayableDirector : DialoguePlayableDirector
{
    [SerializeField]
    private CinemachineVirtualCamera speechCamera;

    private CinemachineTrackedDolly dollyTrack;
    private CinemachinePathBase dollyPath;

    private bool fieldsInitialized = false;

    public override void SetActor(DialogueActor actor)
    {
        dollyPath.transform.position = actor.CameraPositionTransform.position;
        dollyPath.transform.LookAt(actor.CameraLookAtTransform.position);
        speechCamera.LookAt = actor.CameraLookAtTransform;
    }

    public override void SetRandomPosition()
    {
        dollyTrack.m_PathPosition = dollyTrack.m_Path.MinPos +
            (float)random.NextDouble() * (dollyTrack.m_Path.MaxPos - dollyTrack.m_Path.MinPos);
    }
    
    public override void SetDefaultPosition()
    {
        dollyTrack.m_PathPosition = 0;
    }

    private void InitializeFields()
    {
        if (!fieldsInitialized)
        {
            dollyTrack = speechCamera.GetCinemachineComponent<CinemachineTrackedDolly>();
            dollyPath = dollyTrack.m_Path;
            fieldsInitialized = true;
        }
    }

    protected override void Awake()
    {
        base.Awake();
        InitializeFields();
    }
}