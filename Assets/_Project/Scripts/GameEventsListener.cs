﻿using System;

public class GameEventsListener
{
    public event Action LevelCompleteRequested;

    public void RequestLevelComplete()
    {
        LevelCompleteRequested?.Invoke();
    }
}
