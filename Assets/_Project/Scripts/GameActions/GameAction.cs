﻿using UnityEngine;

public abstract class GameAction : MonoBehaviour
{
    protected GameEventsListener gameEventsListener;

    public void Init(GameEventsListener gameEventsListener)
    {
        this.gameEventsListener = gameEventsListener;
    }

    public abstract void Execute();
}
