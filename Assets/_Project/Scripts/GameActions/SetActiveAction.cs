﻿using UnityEngine;

public class SetActiveAction : GameAction
{
    [SerializeField]
    private bool activeState;
    [SerializeField]
    private GameObject[] objectsToChangeState;

    public override void Execute()
    {
        foreach (var item in objectsToChangeState)
        {
            item.SetActive(activeState);
        }
    }
}
