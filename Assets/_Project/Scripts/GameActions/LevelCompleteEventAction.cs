﻿public class LevelCompleteEventAction : GameAction
{
    public override void Execute()
    {
        gameEventsListener.RequestLevelComplete();
    }
}
