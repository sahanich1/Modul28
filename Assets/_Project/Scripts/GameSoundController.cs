﻿using UnityEngine;
using UnityEngine.Audio;

/// <summary>
/// Контроллер звукового сопровождения игры
/// </summary>
public class GameSoundController : MonoBehaviour
{
    [SerializeField]
    private AudioMixer audioMixer;
    [SerializeField]
    private AudioSource[] pausableAudioSources;

    private StoredGameDataManager _storedGameDataManager;
    private UIEventMediator _uiEventMediator;

    public void Init(StoredGameDataManager storedGameDataManager, UIEventMediator uiEventMediator)
    {
        _uiEventMediator = uiEventMediator;
        _uiEventMediator.ApplyToAudioMixerRequested += ApplyToAudioMixer;
        _storedGameDataManager = storedGameDataManager;
        ApplyAllToAudioMixer();
    }

    public void Resume()
    {
        foreach (var item in pausableAudioSources)
        {
            item.Play();
        }
    }

    public void Pause()
    {
        foreach (var item in pausableAudioSources)
        {
            item.Pause();
        }
    }

    public void ApplyAllToAudioMixer()
    {
        _storedGameDataManager.SoundOptions.ApplyAllToAudioMixer(audioMixer);
    }

    public void ApplyToAudioMixer(string paramName)
    {
        SoundOptions soundOptions = _storedGameDataManager.SoundOptions;
        soundOptions.ApplyToAudioMixer(audioMixer, paramName);
    }
}
