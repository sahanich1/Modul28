using UnityEngine;

public enum GameMode
{
    Gameplay = 1,
    Dialogue = 2,
    Cutscene = 3,
    Menu = 4,
}

public class GameController : MonoBehaviour
{
    [SerializeField]
    private Actor player;
    [SerializeField]
    private UIScreenController uiScreenController;
    [SerializeField]
    private GameSoundController gameSoundController;
    [SerializeField]
    private FreeLookCameraController freeLookCameraController;
    [SerializeField]
    private DialogueController dialogueController;
    [SerializeField]
    private CutsceneController cutsceneController;
    [SerializeField]
    private AimController aimController;

    [Header("Views")]
    [SerializeField]
    private DialogueDisplay dialogueDisplay;
    [SerializeField]
    private AimDisplay aimDisplay;

    private GameEventsListener gameEventsListener;
    private UIEventMediator uiEventMediator;
    private PlayerControl playerControl;
    private KeyPressController keyPressController;
    private CursorController cursorController;
    private StoredGameDataManager storedGameDataManager;

    private GameMode currentGameMode;
    private GameMode previousGameMode;

    private void Awake()
    {
        gameEventsListener = new GameEventsListener();
        uiEventMediator = new UIEventMediator();
        playerControl = new PlayerControl();
        keyPressController = new KeyPressController();
        cursorController = new CursorController();
        storedGameDataManager = new StoredGameDataManager();
    }

    private void Start()
    {
        keyPressController.Init(uiEventMediator, uiScreenController);
        cursorController.Init(dialogueController);
        gameSoundController.Init(storedGameDataManager, uiEventMediator);
        aimController.Init(freeLookCameraController, aimDisplay, player);
        dialogueDisplay.Init(uiEventMediator, dialogueController);
        dialogueController.Init(gameEventsListener, freeLookCameraController);
        uiScreenController.Init(storedGameDataManager, uiEventMediator);

        playerControl.Init(uiEventMediator, aimController, freeLookCameraController, player);

        SetGameMode(GameMode.Cutscene);
        freeLookCameraController.ResetPosition();
        cutsceneController.StartCutsceneEnded += OnStartCutsceneEnded;
        cutsceneController.PlayStartGameCutscene();
    }

    private void OnEnable()
    {
        uiEventMediator.QuitRequested += OnQuitRequested;
        uiEventMediator.PauseGameRequested += OnPauseGameRequested;
        uiEventMediator.ResumeGameRequested += OnResumeGameRequested;
        uiEventMediator.DialogueStartRequested += OnDialogueStartRequested;

        dialogueController.DialogueEnded += OnDialogueControllerDialogueEnded;

        gameEventsListener.LevelCompleteRequested += OnGameCompleted;
    }

    private void OnDisable()
    {
        uiEventMediator.QuitRequested -= OnQuitRequested;
        uiEventMediator.PauseGameRequested -= OnPauseGameRequested;
        uiEventMediator.ResumeGameRequested -= OnResumeGameRequested;
        uiEventMediator.DialogueStartRequested -= OnDialogueStartRequested;

        dialogueController.DialogueEnded -= OnDialogueControllerDialogueEnded;

        gameEventsListener.LevelCompleteRequested -= OnGameCompleted;
    }

    private void Update()
    {
        keyPressController.DoUpdate();

        if (currentGameMode == GameMode.Gameplay)
        {
            aimController.RefreshTarget();
        }
    }

    private void OnPauseGameRequested()
    {
        uiScreenController.ShowPauseScreen();
        gameSoundController.Pause();
        previousGameMode = currentGameMode;
        SetGameMode(GameMode.Menu);
        Time.timeScale = 0;
    }

    private void OnResumeGameRequested()
    {
        uiScreenController.HideCurrentScreen();
        gameSoundController.Resume();
        SetGameMode(previousGameMode);
        Time.timeScale = 1;
    }

    private void OnQuitRequested()
    {
        Application.Quit();
    }

    private void SetGameMode(GameMode gameMode)
    {
        currentGameMode = gameMode;
        freeLookCameraController.SetControlActive(currentGameMode == GameMode.Gameplay);
        keyPressController.SetMode(gameMode);
        cursorController.SetMode(gameMode);
    }

    private void OnDialogueStartRequested(Actor actor)
    {
        SetGameMode(GameMode.Dialogue);
        aimController.ResetTarget();
        dialogueController.StartDialogueAsync(player, actor);
    }

    private void OnDialogueControllerDialogueEnded()
    {
        SetGameMode(GameMode.Gameplay);
    }

    private void OnStartCutsceneEnded()
    {
        cutsceneController.StartCutsceneEnded -= OnStartCutsceneEnded;
        SetGameMode(GameMode.Gameplay);
    }

    private void OnEndCutsceneEnded()
    {
        cutsceneController.EndCutsceneEnded -= OnEndCutsceneEnded;
        Application.Quit();
    }

    private void OnGameCompleted()
    {
        dialogueController.DialogueEnded -= OnDialogueControllerDialogueEnded;

        SetGameMode(GameMode.Cutscene);
        cutsceneController.EndCutsceneEnded += OnEndCutsceneEnded;
        cutsceneController.PlayEndGameCutscene();
    }
}
