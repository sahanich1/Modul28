﻿using UnityEngine;

/// <summary>
/// Сохраняемые в долговременном хранилище настройки игры
/// </summary>
public class StoredGameDataManager
{
    /// <summary>
    /// Параметры звуков
    /// </summary>
    public SoundOptions SoundOptions { get; }

    public StoredGameDataManager()
    {
        SoundOptions = new SoundOptions();
        SoundOptions.LoadFromStorage();
    }
}
