using UnityEngine;

public class SimpleThirdPersonCharacter : MonoBehaviour
{
    [SerializeField] CharacterController cc;
    [SerializeField] Animator m_Animator;
    [SerializeField] float m_MovingTurnSpeed = 360;
    [SerializeField] float m_StationaryTurnSpeed = 180;
    [SerializeField] float m_AnimSpeedMultiplier = 1f;

    float m_TurnAmount;
    float m_ForwardAmount;

    public void Move(Vector3 move)
    {
        if (move.sqrMagnitude > 1f)
        {
            move.Normalize();
        }

        move = transform.InverseTransformDirection(move);
        move.y = 0;

        m_TurnAmount = Mathf.Atan2(move.x, move.z);
        m_ForwardAmount = move.z;

        ApplyExtraTurnRotation();

        UpdateAnimator(move);
    }

    void UpdateAnimator(Vector3 move)
    {
        m_Animator.SetFloat("Forward", m_ForwardAmount, 0.1f, Time.deltaTime);
        m_Animator.SetFloat("Turn", m_TurnAmount, 0.1f, Time.deltaTime);

        if (move.sqrMagnitude > 0)
        {
            m_Animator.speed = m_AnimSpeedMultiplier;
        }
        else
        {
            m_Animator.speed = 1;
        }
    }

    void ApplyExtraTurnRotation()
    {
        float turnSpeed = Mathf.Lerp(m_StationaryTurnSpeed, m_MovingTurnSpeed, m_ForwardAmount);
        transform.Rotate(0, m_TurnAmount * turnSpeed * Time.deltaTime, 0);
    }
}

