﻿using UnityEngine;

public class SimpleThirdPersonUserControl : MonoBehaviour
{
    [SerializeField]
    private SimpleThirdPersonCharacter m_Character;
    private Transform m_Cam;                  
    private Vector3 m_CamForward;            
    private Vector3 m_Move;


    private void Start()
    {
        m_Cam = Camera.main.transform;
    }

    private void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        if (m_Cam != null)
        {
            m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
            m_Move = v * m_CamForward + h * m_Cam.right;
        }
        else
        {
            m_Move = v * Vector3.forward + h * Vector3.right;
        }
    }

    private void FixedUpdate()
    {
        m_Character.Move(m_Move);
    }
}

