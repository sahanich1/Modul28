using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class SpeechPlayableDirector : DialoguePlayableDirector
{
    [SerializeField]
    private CinemachineVirtualCamera speechCamera;

    [field: SerializeField]
    public DialogueCameraType CameraType { get; private set; }

    private CinemachineTrackedDolly dollyTrack;
    private CinemachinePathBase dollyPath;

    private bool fieldsInitialized;

    protected override void Awake()
    {
        base.Awake();
        InitializeFields();
    }

    public override void SetActor(DialogueActor actor)
    {
        base.SetActor(actor);
        dollyPath.transform.position = actor.CameraPositionTransform.position;
        dollyPath.transform.LookAt(actor.CameraLookAtTransform.position);
        speechCamera.LookAt = actor.CameraLookAtTransform;
    }

    public override void SetRandomPosition()
    {
        base.SetRandomPosition();
        dollyTrack.m_PathPosition = dollyTrack.m_Path.MinPos +
            (float)random.NextDouble() * (dollyTrack.m_Path.MaxPos - dollyTrack.m_Path.MinPos);
        
    }

    public override void SetDefaultPosition()
    {
        base.SetDefaultPosition();
        dollyTrack.m_PathPosition = dollyTrack.m_Path.MinPos;
    }

    private void InitializeFields()
    {
        if (!fieldsInitialized)
        {
            dollyTrack = speechCamera.GetCinemachineComponent<CinemachineTrackedDolly>();
            dollyPath = dollyTrack.m_Path;
            fieldsInitialized = true;
        }
    }
}
