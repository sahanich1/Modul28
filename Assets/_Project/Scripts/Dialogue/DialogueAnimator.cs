using System.Collections;
using UnityEngine;

public class DialogueAnimator : MonoBehaviour
{
    protected const float TransitionTimeBetweenLayers = 2f;
    protected const float ParameterChangeValueVelocity = 0.1f;
    protected const float ParameterChangeValueAccuracy = 0.01f;

    [SerializeField]
    private DialogueActor dialogueActor;
    [SerializeField]
    private Animator animator;

    protected Coroutine animationCoroutine = null;

    private void OnEnable()
    {
        dialogueActor.DialogueStarted += OnDialogueStarted;
        dialogueActor.DialogueEnded += OnDialogueEnded;
        dialogueActor.SpeechStarted += OnSpeechStarted;
        dialogueActor.SpeechEnded += OnSpeechEnded;

        OnDialogueEnded();
        OnSpeechEnded();
    }

    private void OnDisable()
    {
        dialogueActor.DialogueStarted -= OnDialogueStarted;
        dialogueActor.DialogueEnded -= OnDialogueEnded;
        dialogueActor.SpeechStarted -= OnSpeechStarted;
        dialogueActor.SpeechEnded -= OnSpeechEnded;
    }

    private void SetParameterValueSmooth(string paramName, float paramValue)
    {
        if (animator == null || !IsParamaterExists(paramName))
        {
            return;
        }

        float currentValue = animator.GetFloat(paramName);
        int nearestIntValue = Mathf.RoundToInt(currentValue);
        if (Mathf.Abs(currentValue - nearestIntValue) < ParameterChangeValueAccuracy
             && paramValue == nearestIntValue)
        {
            animator.SetFloat(paramName, nearestIntValue);
        }
        else
        {
            animator.SetFloat(paramName, paramValue, ParameterChangeValueVelocity, Time.deltaTime);
        }
    }

    private void SetParameterValue(string paramName, float paramValue)
    {
        if (animator == null || !IsParamaterExists(paramName))
        {
            return;
        }

        animator.SetFloat(paramName, paramValue);
    }

    private void SetParameterValue(string paramName, bool paramValue)
    {
        if (animator == null || !IsParamaterExists(paramName))
        {
            return;
        }

        animator.SetBool(paramName, paramValue);
    }

    private void SetTrigger(string triggerName)
    {
        if (animator == null || !IsParamaterExists(triggerName))
        {
            return;
        }

        animator.SetTrigger(triggerName);
    }

    private IEnumerator AnimateAction(string parameter, float actionTime)
    {
        if (animator == null || actionTime <= 0 || !IsParamaterExists(parameter))
        {
            animationCoroutine = null;
            yield break;
        }

        animator.SetBool(parameter, true);
        yield return new WaitForSeconds(actionTime);
        animator.SetBool(parameter, false);

        animationCoroutine = null;
    }

    private IEnumerator AnimateLayer(string layerName, int targetWeight)
    {
        if (animator == null || !IsLayerExists(layerName))
        {
            animationCoroutine = null;
            yield break;
        }

        int layerIndex = animator.GetLayerIndex(layerName);

        float currentWeight = animator.GetLayerWeight(layerIndex);
        if (targetWeight == currentWeight)
        {
            yield break;
        }

        int weightIncreaseMultiplier = targetWeight > currentWeight ? 1 : -1;

        float currentTransitionTime = 0;
        while (currentTransitionTime < TransitionTimeBetweenLayers)
        {
            yield return null;
            currentTransitionTime += Time.deltaTime;
            currentWeight += weightIncreaseMultiplier * Time.deltaTime / TransitionTimeBetweenLayers;
            animator.SetLayerWeight(layerIndex, currentWeight);
        }
        animator.SetLayerWeight(layerIndex, targetWeight);

        animationCoroutine = null;
    }

    private bool IsParamaterExists(string paramName)
    {
        for (int i = 0; i < animator.parameterCount; i++)
        {
            if (animator.parameters[i].name == paramName)
            {
                return true;
            }
        }
        return false;
    }

    private bool IsLayerExists(string paramName)
    {
        for (int i = 0; i < animator.layerCount; i++)
        {
            if (animator.GetLayerName(i) == paramName)
            {
                return true;
            }
        }
        return false;
    }

    private void OnDialogueStarted()
    {
        SetTrigger(Constants.Animation.Parameter.DialogueModeEnterTrigger);
        SetParameterValue(Constants.Animation.Parameter.DialogueModeBool, true);
    }

    private void OnDialogueEnded()
    {
        SetParameterValue(Constants.Animation.Parameter.DialogueModeBool, false);
    }

    private void OnSpeechStarted()
    {
        SetParameterValue(Constants.Animation.Parameter.SpeechPlayingBool, true);
        SetTrigger(Constants.Animation.Parameter.DialoguePlayRandomTrigger);
        SetLayerWeightSmoothly(Constants.Animation.Layer.UpperBody, 1);
    }

    private void OnSpeechEnded()
    {
        SetParameterValue(Constants.Animation.Parameter.SpeechPlayingBool, false);
        SetLayerWeightSmoothly(Constants.Animation.Layer.UpperBody, 0);
    }

    private void SetLayerWeightSmoothly(string layerName, int targetWeight)
    {
        StartCoroutine(AnimateLayer(layerName, targetWeight));
    }
}


