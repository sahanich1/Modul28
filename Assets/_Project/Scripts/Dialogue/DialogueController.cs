using System;
using System.Collections;
using UnityEngine;
using System.Linq;
using System.Threading.Tasks;

public class DialogueController : MonoBehaviour
{
    private const float StartDialogueMinCameraAngle = 45;
    private const float AverageWordLength = 6;
    private const float MinSpeechPlaybackTime = 2;

    [SerializeField]
    private float wordsPerMinuteReadingSpeed = 150;
    [SerializeField]
    private float changeCameraDelay = 10;
    [SerializeField]
    private DialoguePlayableDirector startDialoguePlayback;
    [SerializeField]
    private DialoguePlayableDirector endDialoguePlayback;
    [SerializeField]
    private SpeechPlayableDirector defaultSpeechTimeline;
    [SerializeField]
    private SpeechPlayableDirector[] speechTimelines;

    private System.Random random = new System.Random();
    private float symbolsPerSecondReadingSpeed;
    private bool isFirstSpeech;

    private DialoguePlayableDirector currentPlayback;
    private Dialogue currentDialogue;
    private bool speechInterruptRequested;

    private Coroutine speechPlaybackCoroutine;
    private Coroutine playingSpeechesCoroutine;   

    private Actor player;
    private Actor npc;
    private Vector3 initialNpcForward;

    public event Action<DialogueSpeech> SpeechStarted;
    public event Action<DialogueSpeech> SpeechEnded;
    public event Action<DialogueSpeech> WaitingForChooseVariant;
    public event Action DialogueStarted;
    public event Action DialogueEnded;

    private GameEventsListener gameEventsListener;
    private FreeLookCameraController freeLookCameraController;

    private void Awake()
    {
        symbolsPerSecondReadingSpeed = wordsPerMinuteReadingSpeed * AverageWordLength / 60;
    }


    public void Init(GameEventsListener gameEventsListener, FreeLookCameraController freeLookCameraController)
    {
        this.gameEventsListener = gameEventsListener;
        this.freeLookCameraController = freeLookCameraController;
    }

    public DialogueSpeech GetSpeech(int speechID)
    {
        return currentDialogue.GetSpeech(speechID);
    }

    public void ChooseVariant(int speechID)
    {
        currentDialogue.SetSpeech(speechID);
        PlaySpeeches();
    }

    public void SkipSpeech()
    {
        speechInterruptRequested = true;
    }

    public async void StartDialogueAsync(Actor player, Actor npc)
    {
        this.player = player;
        this.npc = npc;

        float distanceBetweenActors = npc.DistanceToCamera > player.DistanceToCamera
            ? npc.DistanceToCamera : player.DistanceToCamera;

        Vector3 npcForwardDirection = player.transform.position - npc.transform.position;
        npcForwardDirection.y = 0;
        npcForwardDirection = npcForwardDirection.normalized;

        Vector3 playerDestinationPoint = npc.transform.position + npcForwardDirection * distanceBetweenActors;
        playerDestinationPoint.y = player.transform.position.y;

        npc.StopMoving();
        player.StopMoving();
        player.MoveBody(playerDestinationPoint - player.transform.position);

        initialNpcForward = npc.transform.forward;
        player.transform.LookAt(new Vector3(npc.transform.position.x, player.transform.position.y, npc.transform.position.z));
        npc.transform.LookAt(new Vector3(player.transform.position.x, npc.transform.position.y, player.transform.position.z));

        currentDialogue = new Dialogue(player.DialogueBehaviour, npc.DialogueBehaviour);

        Task task = currentDialogue.LoadDialogueAsync();
        if (!task.IsCompleted)
        {
            await task;
        }

        player.StartDialogue(this);
        npc.DialogueBehaviour.Init(gameEventsListener);
        npc.StartDialogue(this);

        StartCoroutine(PerformDialogueStart());
    }

    private void StopDialogueSmoothly()
    {
        StopPlayingSpeeches();
        if (npc.DialogueBehaviour.RestoreInitialRotationAfterDialogue)
        {
            npc.transform.forward = initialNpcForward;
        }

        npc.DialogueBehaviour.ExecuteDialogueEndActions();
        StartCoroutine(PerformDialogueEnd());
    }

    private void StopCurrentPlayback()
    {
        if (currentPlayback != null)
        {
            currentPlayback.Stop();
            currentPlayback = null;
        }
    }

    private void PlaySpeeches()
    {
        if (playingSpeechesCoroutine != null)
        {
            StopCoroutine(playingSpeechesCoroutine);
        }
        playingSpeechesCoroutine = StartCoroutine(PerformPlayingSpeeches());
    }

    private void StopPlayingSpeeches()
    {
        if (speechPlaybackCoroutine != null)
        {
            StopCoroutine(speechPlaybackCoroutine);
            speechPlaybackCoroutine = null;
        }
    }

    private IEnumerator PerformPlayingSpeeches()
    {
        bool nextSpeechGetted = currentDialogue.CurrentSpeech != null;
        while (nextSpeechGetted)
        {
            SpeechStarted?.Invoke(currentDialogue.CurrentSpeech);
            if (!isFirstSpeech)
            {
                PlaySpeech(currentDialogue.CurrentSpeech);
            }
            isFirstSpeech = false;
            
            speechInterruptRequested = false;           
            float speechTime = currentDialogue.CurrentSpeech.Text.Length / symbolsPerSecondReadingSpeed;
            if (speechTime < MinSpeechPlaybackTime)
            {
                speechTime = MinSpeechPlaybackTime;
            }
            float endSpeechTime = Time.time + speechTime;
            while (Time.time < endSpeechTime && !speechInterruptRequested)
            {
                yield return null;
            }
            SpeechEnded?.Invoke(currentDialogue.CurrentSpeech);
            nextSpeechGetted = currentDialogue.NextSpeech();          
        }

        if (currentDialogue.IsThereVariantsInCurrentSpeech())
        {
            WaitingForChooseVariant?.Invoke(currentDialogue.CurrentSpeech);
        }
        else
        {
            StopDialogueSmoothly();
        }

        playingSpeechesCoroutine = null;
    }

    private void PlaySpeech(DialogueSpeech speech)
    {
        if (speechPlaybackCoroutine != null)
        {
            StopCoroutine(speechPlaybackCoroutine);
        }
        speechPlaybackCoroutine = StartCoroutine(RunSpeechPlayback(speech));
    }

    private DialoguePlayableDirector GetRandomPlayback(DialoguePlayableDirector defaultTimeline,
        DialoguePlayableDirector[] timelines)
    {
        if (timelines.Length == 0)
        {
            return defaultTimeline;
        }
        return timelines[random.Next(timelines.Length)];        
    }

    private void CorrectFreeLookCameraForDialogueStart()
    {
        Vector3 playerForward = player.transform.forward;
        playerForward.y = 0;

        Vector3 cameraForward = freeLookCameraController.MainCamera.transform.forward;
        cameraForward.y = 0;

        float playerCameraAngle = Vector3.SignedAngle(playerForward, cameraForward, Vector3.up);

        if (Mathf.Abs(playerCameraAngle) < StartDialogueMinCameraAngle)
        {
            if (playerCameraAngle > 0)
            {
                freeLookCameraController.FreeLookCam.m_XAxis.Value = StartDialogueMinCameraAngle - playerCameraAngle;
            }
            else
            {
                freeLookCameraController.FreeLookCam.m_XAxis.Value = playerCameraAngle - StartDialogueMinCameraAngle;
            }
        }
    }

    private void CorrectFreeLookCameraForDialogueEnd()
    {
        Vector3 playerForward = player.transform.forward;
        playerForward.y = 0;

        Vector3 cameraForward = freeLookCameraController.MainCamera.transform.forward;
        cameraForward.y = 0;

        Vector3 freeLookCameraForward = freeLookCameraController.FreeLookCam.LookAt.position
            - freeLookCameraController.FreeLookCam.transform.position;
        freeLookCameraForward.y = 0;

        float playerCameraAngle = Vector3.SignedAngle(-playerForward, -cameraForward, Vector3.up);
        float playerFreeLookCameraAngle = Vector3.SignedAngle(-playerForward, -freeLookCameraForward, Vector3.up);

        if (playerCameraAngle * playerFreeLookCameraAngle < 0)
        {
            freeLookCameraController.FreeLookCam.m_XAxis.Value = -2 * playerFreeLookCameraAngle;
        }
    }

    private IEnumerator PerformDialogueStart()
    {
        DialogueStarted?.Invoke();
        
        StopCurrentPlayback();
        currentPlayback = startDialoguePlayback;
        currentPlayback.SetActor(currentDialogue.CurrentSpeech.Actor);
        currentPlayback.SetDefaultPosition();
        currentPlayback.Play();

        CorrectFreeLookCameraForDialogueStart();

        yield return new WaitForSeconds(currentPlayback.GetPlaybackTime());
        
        isFirstSpeech = true;
        PlaySpeeches();
    }

    private IEnumerator PerformDialogueEnd()
    {
        currentPlayback.SetActor(currentDialogue.Player);
        yield return null;

        StopCurrentPlayback();
        currentPlayback = endDialoguePlayback;
        currentPlayback.Play();

        // Need wait for one frame to free look camera become live
        yield return null;
        CorrectFreeLookCameraForDialogueEnd();
        yield return new WaitForSeconds(currentPlayback.GetPlaybackTime());
        StopCurrentPlayback();
        DialogueEnded?.Invoke();

        player.StopDialogue(this);
        npc.StopDialogue(this);
    }

    private IEnumerator RunSpeechPlayback(DialogueSpeech speech)
    {
        yield return new WaitForSeconds(0.2f);

        SpeechPlayableDirector[] allowedSpeechTimelines = speechTimelines.Where
            ((t) => currentDialogue.NPC.AllowedCameraType.HasFlag(t.CameraType)).ToArray();
        DialogueActor currentActor = speech.Actor;
        bool isFirstIteration = true;
        while (speechPlaybackCoroutine != null)
        {            
            StopCurrentPlayback();
            if (isFirstIteration)
            {
                currentPlayback = defaultSpeechTimeline;
                isFirstIteration = false;
            }
            else
            {
                currentPlayback = GetRandomPlayback(defaultSpeechTimeline, allowedSpeechTimelines);
                currentActor = currentActor == currentDialogue.Player ? currentDialogue.NPC : currentDialogue.Player;
            }

            currentPlayback.SetActor(currentActor);
            currentPlayback.SetRandomPosition();
            currentPlayback.Play();

            yield return new WaitForSeconds(changeCameraDelay);
        }
        
    }
}
