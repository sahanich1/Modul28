﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class Dialogue
{
    private Dictionary<int, DialogueSpeech> dialogueSpeeches;

    public DialogueSpeech CurrentSpeech { get; private set; }
    public DialogueActor Player { get; private set; }
    public DialogueActor NPC { get; private set; }


    public Dialogue(DialogueActor playerDialogueActor, DialogueActor npcDialogueActor)
    {
        Player = playerDialogueActor;
        NPC = npcDialogueActor;
    }

    public async Task LoadDialogueAsync()
    {
        CurrentSpeech = null;
        Task<Dictionary<int, DialogueSpeech>> task = DialogueFile.ReadDialogueSpeeches(NPC.DialogueFileName,
            Player, NPC);
        if (!task.IsCompleted)
        {
            await task;
        }
        dialogueSpeeches = task.Result;

        SetSpeech(0);
    }

    public bool NextSpeech()
    {
        if (CurrentSpeech == null)
        {
            // Текущей реплики нет, значит диалог только начался. Выбираем стартовую реплику
            CurrentSpeech = dialogueSpeeches[0];
            return true;
        }
        else if (CurrentSpeech.NextSpeechVariantsId.Length == 1)
        {
            // Если есть только один вариант продолжения беседы, сразу выбираем его.
            CurrentSpeech = dialogueSpeeches[CurrentSpeech.NextSpeechVariantsId[0]];
            return true;
        }

        // Вариантов продолжения беседы нет или есть несколько вариантов. 
        // В любом случае следующая реплика не может быть выбрана автоматически.
        return false;
    }

    public bool IsThereVariantsInCurrentSpeech()
    {
        return CurrentSpeech != null && CurrentSpeech.NextSpeechVariantsId.Length > 0;
    }

    public void SetSpeech(int speechId)
    {
        CurrentSpeech = dialogueSpeeches[speechId];
    }
	
    public DialogueSpeech GetSpeech(int speechId)
    {
        return dialogueSpeeches[speechId];
    }
}
