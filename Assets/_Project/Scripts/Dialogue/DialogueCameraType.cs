﻿[System.Flags]
public enum DialogueCameraType
{
    Static = 1,
    Circular = 2
}
