﻿using UnityEngine;
using UnityEngine.Playables;

[RequireComponent(typeof(PlayableDirector))]
public abstract class DialoguePlayableDirector : MonoBehaviour
{
    protected PlayableDirector timeline;

    protected System.Random random = new System.Random();

    protected virtual void Awake()
    {
        timeline = GetComponent<PlayableDirector>();
    }

    public virtual void Play()
    {
        timeline.Play();
    }

    public virtual void Stop()
    {
        timeline.Stop();
    }

    public float GetPlaybackTime()
    {
        return (float)timeline.duration;
    }

    public virtual void SetActor(DialogueActor actor)
    {
    }

    public virtual void SetRandomPosition()
    {
        timeline.time = timeline.initialTime + (float)random.NextDouble() * (timeline.duration - timeline.initialTime);
    }

    public virtual void SetDefaultPosition()
    {
        timeline.time = timeline.initialTime;
    }
}
