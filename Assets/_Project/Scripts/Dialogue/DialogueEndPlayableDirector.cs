﻿using UnityEngine;
using Cinemachine;

public class DialogueEndPlayableDirector : DialoguePlayableDirector
{
    [SerializeField]
    private CinemachineFreeLook freeLookCamera;

    public override void Play()
    {
        freeLookCamera.PreviousStateIsValid = false;
        freeLookCamera.m_XAxis.Value = 0;
        freeLookCamera.m_YAxis.Value = 0.5f;
        base.Play();
    }
}
