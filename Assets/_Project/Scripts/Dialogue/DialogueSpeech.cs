﻿public class DialogueSpeech
{
    public DialogueActor Actor { get; set; }
    public int Id { get; set; }
    public string Text { get; set; }
    public int[] NextSpeechVariantsId { get; set; }

    public DialogueSpeech(DialogueActor actor, int id, string text, int[] nextSpeechVariantsId)
    {
        Actor = actor;
        Id = id;
        Text = text;
        NextSpeechVariantsId = nextSpeechVariantsId;
    }

}
