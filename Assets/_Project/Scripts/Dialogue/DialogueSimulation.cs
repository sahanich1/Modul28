﻿using System;
using System.Collections;
using UnityEngine;

public class DialogueSimulation : MonoBehaviour
{
    [SerializeField]
    private float minSpeechTime = 3;
    [SerializeField]
    private float maxSpeechTime = 6;
    [SerializeField]
    private DialogueActor[] actors;

    private readonly System.Random random = new();
    private DialogueActor currentSpeakingActor;

    private Coroutine dialogueRoutine;

    private void Awake()
    {
        foreach (var actor in actors)
        {
            actor.StartDialogue();
        }

        dialogueRoutine = StartCoroutine(PerformDialogueSimulation());
    }

    private void OnEnable()
    {
        foreach (var actor in actors)
        {
            actor.DialogueStarted += OnActorDialogueStarted;
            actor.DialogueEnded += OnActorDialogueEnded;
        }
    }

    private void OnDisable()
    {
        foreach (var actor in actors)
        {
            actor.DialogueStarted -= OnActorDialogueStarted;
            actor.DialogueEnded -= OnActorDialogueEnded;
        }
    }

    private IEnumerator PerformDialogueSimulation()
    {
        while (true)
        {
            if (currentSpeakingActor != null)
            {
                currentSpeakingActor.StopSpeech();
            }
            currentSpeakingActor = actors[random.Next(actors.Length)];
            currentSpeakingActor.StartSpeech();

            yield return new WaitForSeconds(minSpeechTime + (maxSpeechTime - minSpeechTime) * (float)random.NextDouble());
        }
    }

    private void OnActorDialogueStarted()
    {
        StopCoroutine(dialogueRoutine);
        foreach (var actor in actors)
        {
            actor.StopSpeech();
        }
        currentSpeakingActor = null;
    }

    private void OnActorDialogueEnded()
    {
        dialogueRoutine = StartCoroutine(PerformDialogueSimulation());
    }
}
