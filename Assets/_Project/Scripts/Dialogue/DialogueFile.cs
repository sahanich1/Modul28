using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public static class DialogueFile
{
    private const string Extension = ".txt";
    private const string Folder = "Dialogues";
    private const string NpcTag = "npc";
    private const string NpcNameTag = "npcName";

    public static async Task<Dictionary<int, DialogueSpeech>> ReadDialogueSpeeches(string fileName,
        DialogueActor playerDialogueActor, DialogueActor npcDialogueActor)
    {
        Dictionary<int, DialogueSpeech> dialogueSpeeches = new Dictionary<int, DialogueSpeech>();

        try
        {
            string path = Path.Combine(new string[] { Application.streamingAssetsPath, Folder, fileName }) + Extension;
            FileInfo fileInfo = new FileInfo(path);
            if (!fileInfo.Exists)
            {
                Debug.Log($"'{fileName}' does not exists.");
                return dialogueSpeeches;
            }

            using (StreamReader sr = new StreamReader(path, Encoding.UTF8))
            {
                Task<string> task = sr.ReadToEndAsync();
                if (!task.IsCompleted)
                {
                    await task;
                }

                string[] lines = task.Result.Split(Environment.NewLine);
                foreach (var line in lines)
                {
                    int[] variantIds = GetVariantSpeechIds(line);
                    string speechText = GetSpeechText(line, npcDialogueActor);

                    int speechId = GetSpeechId(line);
                    DialogueActor actor = GetSpeechActor(line, playerDialogueActor, npcDialogueActor);

                    DialogueSpeech dialogueSpeech = new DialogueSpeech(actor, speechId, speechText, variantIds);
                    dialogueSpeeches.Add(speechId, dialogueSpeech);
                }
            }

        }
        catch (Exception e)
        {
            Debug.Log($"{e}");
            return dialogueSpeeches;
        }

        return dialogueSpeeches;
    }

    private static string GetSubstringBetweenChars(string text, char char1, char char2)
    {
        int substringStartIndex = text.IndexOf(char1) + 1;
        int substringEndIndex = text.IndexOf(char2) - 1;
        if (substringStartIndex < 0 || substringEndIndex < 0)
        {
            return "";
        }
        return text.Substring(substringStartIndex, substringEndIndex - substringStartIndex + 1);
    }

    private static int[] GetVariantSpeechIds(string text)
    {
        string variantsString = GetSubstringBetweenChars(text, '[', ']');
        string[] variantsNumberStringArray = variantsString.Split(new char[] { ',' },
            StringSplitOptions.RemoveEmptyEntries);

        int[] variantIdArray = new int[variantsNumberStringArray.Length];
        for (int i = 0; i < variantsNumberStringArray.Length; i++)
        {
            variantIdArray[i] = int.Parse(variantsNumberStringArray[i]);
        }

        return variantIdArray;
    }

    private static string GetSpeechText(string text, DialogueActor npcDialogueActor)
    {
        int speechTextStartIndex = text.IndexOf('>');
        if (speechTextStartIndex < 0)
        {
            return "";
        }
        string speechText = text.Substring(speechTextStartIndex + 1);
        return speechText.Replace($"{{{NpcNameTag}}}", npcDialogueActor.ActorName);
    }

    private static DialogueActor GetSpeechActor(string text, DialogueActor playerDialogueActor, DialogueActor npcDialogueActor)
    {
        if (text.Contains($"{{{NpcTag}}}"))
        {
            return npcDialogueActor;
        }
        else
        {
            return playerDialogueActor;
        }
    }

    private static int GetSpeechId(string text)
    {
        string idString = GetSubstringBetweenChars(text, '<', '>');
        int.TryParse(idString, out int id);
        return id;
    }
}
