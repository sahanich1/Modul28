using System;
using UnityEngine;

public class DialogueActor : MonoBehaviour
{
    [SerializeField]
    private string dialogueFileName;
    [SerializeField]
    private string actorName;
    [SerializeField]
    private Transform cameraLookAtTransform;
    [SerializeField]
    private Transform cameraPositionTransform;
    [SerializeField]
    private GameAction[] dialogueEndActions;

    [field: SerializeField]
    public bool RestoreInitialRotationAfterDialogue { get; private set; } = true;
    [field: SerializeField]
    public DialogueCameraType AllowedCameraType { get; private set; }

    public string ActorName => actorName;
    public string DialogueFileName => dialogueFileName;
    public Transform CameraLookAtTransform => cameraLookAtTransform;
    public Transform CameraPositionTransform => cameraPositionTransform;

    public event Action SpeechStarted;
    public event Action SpeechEnded;
    public event Action DialogueStarted;
    public event Action DialogueEnded;

    public void Init(GameEventsListener gameEventsListener)
    {
        if (dialogueEndActions != null)
        {
            foreach (var item in dialogueEndActions)
            {
                item.Init(gameEventsListener);
            }
        }
    }

    public void SubscribeToDialogueEvents(DialogueController dialogueController)
    {
        dialogueController.DialogueStarted += OnDialogueStarted;
        dialogueController.DialogueEnded += OnDialogueEnded;
        dialogueController.SpeechStarted += OnSpeechStarted;
        dialogueController.SpeechEnded += OnSpeechEnded;
    }

    public void UnsubscribeFromDialogueEvents(DialogueController dialogueController)
    {
        dialogueController.DialogueStarted -= OnDialogueStarted;
        dialogueController.DialogueEnded -= OnDialogueEnded;
        dialogueController.SpeechStarted -= OnSpeechStarted;
        dialogueController.SpeechEnded -= OnSpeechEnded;
    }

    public void ExecuteDialogueEndActions()
    {
        if (dialogueEndActions == null)
        {
            return;
        }

        foreach (var item in dialogueEndActions)
        {
            item.Execute();
        }
    }

    public void StartDialogue()
    {
        DialogueStarted?.Invoke();
    }

    public void StopDialogue()
    {
        DialogueEnded?.Invoke();
    }

    public void StartSpeech()
    {
        SpeechStarted?.Invoke();
    }

    public void StopSpeech()
    {
        SpeechEnded?.Invoke();
    }

    private void OnDialogueStarted()
    {
        DialogueStarted?.Invoke();
    }

    private void OnDialogueEnded()
    {
        DialogueEnded?.Invoke();
    }

    private void OnSpeechStarted(DialogueSpeech speech)
    {
        if (speech.Actor != this)
        {
            return;
        }
        SpeechStarted?.Invoke();
    }

    private void OnSpeechEnded(DialogueSpeech speech)
    {
        if (speech.Actor != this)
        {
            return;
        }
        SpeechEnded?.Invoke();
    }
}