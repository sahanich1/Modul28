﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Экран паузы.
/// Скрипт следует добавлять на UI-панель, которая должна появляться, когда игра ставится на паузу.
/// </summary>
public class PauseScreen : UIScreen
{
    [SerializeField]
    [Tooltip("Кнопка возврата в игру")]
    private Button resumeButton = null;

    [SerializeField]
    [Tooltip("Кнопка выхода из игры")]
    private Button exitButton = null;

    [SerializeField]
    private SoundOptionsPanel soundOptionsPanel;

    public override void Init(StoredGameDataManager storedGameDataManager, UIEventMediator uiEventMediator)
    {
        base.Init(storedGameDataManager, uiEventMediator);
        soundOptionsPanel.Init(storedGameDataManager, uiEventMediator);
    }

    private void OnEnable()
    {
        resumeButton.onClick.AddListener(OnResumeButtonClick);
        exitButton.onClick.AddListener(OnExitButtonClick);
    }

    private void OnDisable()
    {
        resumeButton.onClick.RemoveListener(OnResumeButtonClick);
        exitButton.onClick.RemoveListener(OnExitButtonClick);
    }

    private void OnResumeButtonClick()
    {
        EventSystem.current.SetSelectedGameObject(null);
        _uiEventMediator.RequestResumeGame();
    }

    private void OnExitButtonClick()
    {
        EventSystem.current.SetSelectedGameObject(null);
        _uiEventMediator.RequestQuit();
    }
}
