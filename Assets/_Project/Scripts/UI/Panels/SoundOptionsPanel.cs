﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Панель настроек звука
/// </summary>
public class SoundOptionsPanel : MonoBehaviour
{
    [SerializeField, Tooltip("Слайдер громкости звуков")]
    private Slider sfxVolumeSlider = null;
    [SerializeField, Tooltip("Слайдер громкости музыки")]
    private Slider musicVolumeSlider = null;

    /// <summary>
    /// Словарь пар "название параметра громкости" - "слайдер"
    /// </summary>
    private readonly Dictionary<string, Slider> slidersParamNamesDictionary 
        = new Dictionary<string, Slider>();

    private UIEventMediator _uiEventMediator;
    private StoredGameDataManager _storedGameDataManager;

    private void OnEnable()
    {
        SetSlidersByOptions();
    }

    private void OnDisable()
    {
        SaveToStorage();
    }

    public void Init(StoredGameDataManager storedGameDataManager, UIEventMediator uiEventMediator)
    {
        _uiEventMediator = uiEventMediator;
        _storedGameDataManager = storedGameDataManager;

        RegisterVolumeSlider(sfxVolumeSlider, SoundOptions.SfxVolumeParamName);
        RegisterVolumeSlider(musicVolumeSlider, SoundOptions.MusicVolumeParamName);
    }

    private void SaveToStorage()
    {
        _storedGameDataManager?.SoundOptions.SaveToStorage();
    }

    private void SetSlidersByOptions()
    {
        if (_storedGameDataManager == null)
        {
            return;
        }

        SoundOptions soundOptions = _storedGameDataManager.SoundOptions;
        foreach (var item in slidersParamNamesDictionary)
        {
            SoundVolumeData soundVolumeData = soundOptions.GetVolumeData(item.Key);
            if (soundVolumeData != null)
            {
                item.Value.value = soundVolumeData.NormalizedVolume;
            }          
        }
    }

    private void RegisterVolumeSlider(Slider slider, string paramName)
    {
        slidersParamNamesDictionary.Add(paramName, slider);
        slider.onValueChanged.AddListener(
            (value) => OnSliderValueChanged(paramName, value));
    }

    private void OnSliderValueChanged(string paramName, float value)
    {
        SoundVolumeData soundVolumeData = _storedGameDataManager.SoundOptions.GetVolumeData(paramName);
        soundVolumeData.SetNormalizedVolume(value);
        _uiEventMediator.RequestApplyToAudioMixer(paramName);
    }
}
