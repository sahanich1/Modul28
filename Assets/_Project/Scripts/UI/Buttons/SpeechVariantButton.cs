using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SpeechVariantButton : MonoBehaviour
{
    [SerializeField]
    private TMP_Text text;
    [SerializeField]
    private Button button;

    public void Initialize(int buttonNumber, DialogueSpeech speech, Action<int> chooseVariantAction)
    {
        text.text = $"{buttonNumber}. {speech.Text}";
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(() => chooseVariantAction(speech.Id));
    }

    public void Click()
    {
        button.onClick?.Invoke();
    }
}
