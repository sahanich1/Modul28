using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DialogueDisplay : MonoBehaviour
{
    [SerializeField]
    private RectTransform dialoguePanel;
    [SerializeField]
    private RectTransform variantsPanel;
    [SerializeField]
    private TMP_Text speechText;
    [SerializeField]
    private TMP_Text actorNameText;
    [SerializeField]
    private SpeechVariantButton[] speechVariantButtons;

    private DialogueController dialogueController;
    private UIEventMediator uiEventMediator;

    private void Awake()
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate(dialoguePanel);
        LayoutRebuilder.ForceRebuildLayoutImmediate(variantsPanel);
        dialoguePanel.gameObject.SetActive(false);
        variantsPanel.gameObject.SetActive(false);
    }

    public void Init(UIEventMediator uiEventMediator, DialogueController dialogueController)
    {
        OnDisable();

        this.uiEventMediator = uiEventMediator;
        this.dialogueController = dialogueController;

        if (gameObject.activeInHierarchy)
        {
            OnEnable();
        }
    }

    private void OnEnable()
    {
        if (dialogueController != null)
        {
            dialogueController.SpeechStarted += DialogueControllerOnSpeechStarted;
            dialogueController.WaitingForChooseVariant += DialogueControllerWaitingForChooseVariant;
            dialogueController.DialogueEnded += DialogueControllerDialogueEnded;
        }

        if (uiEventMediator != null)
        {
            uiEventMediator.PlayerAnswerRequested += ClickVariantButton;
            uiEventMediator.SkipSpeechRequested += ClickSkipSpeechButton;
        }
    }

    private void OnDisable()
    {
        if (dialogueController != null)
        {
            dialogueController.SpeechStarted -= DialogueControllerOnSpeechStarted;
            dialogueController.WaitingForChooseVariant -= DialogueControllerWaitingForChooseVariant;
            dialogueController.DialogueEnded -= DialogueControllerDialogueEnded;
        }

        if (uiEventMediator != null)
        {
            uiEventMediator.PlayerAnswerRequested -= ClickVariantButton;
            uiEventMediator.SkipSpeechRequested -= ClickSkipSpeechButton;
        }
    }

    private void ClickVariantButton(int buttonIndex)
    {
        if (buttonIndex < 0 || buttonIndex >= speechVariantButtons.Length)
        {
            return;
        }
        if (!speechVariantButtons[buttonIndex].gameObject.activeInHierarchy)
        {
            return;
        }
        speechVariantButtons[buttonIndex].Click();
    }

    private void ClickSkipSpeechButton()
    {
        dialogueController.SkipSpeech();
    }

    private void DialogueControllerOnSpeechStarted(DialogueSpeech currentSpeech)
    {
        actorNameText.text = currentSpeech.Actor.ActorName;
        speechText.text = currentSpeech.Text;

        dialoguePanel.gameObject.SetActive(true);
        variantsPanel.gameObject.SetActive(false);

        LayoutRebuilder.ForceRebuildLayoutImmediate(dialoguePanel);
    }

    private void DialogueControllerDialogueEnded()
    {
        dialoguePanel.gameObject.SetActive(false);
        variantsPanel.gameObject.SetActive(false);
    }

    private void DialogueControllerWaitingForChooseVariant(DialogueSpeech currentSpeech)
    {
        for (int i = 0; i < speechVariantButtons.Length; i++)
        {
            speechVariantButtons[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < currentSpeech.NextSpeechVariantsId.Length; i++)
        {
            speechVariantButtons[i].gameObject.SetActive(true);
            DialogueSpeech variantSpeech = dialogueController.GetSpeech(currentSpeech.NextSpeechVariantsId[i]);
            speechVariantButtons[i].Initialize(i + 1, variantSpeech, OnVariantButtonClick);
        }

        variantsPanel.gameObject.SetActive(true);
        LayoutRebuilder.ForceRebuildLayoutImmediate(variantsPanel);
        EventSystem.current.SetSelectedGameObject(null);
    }
    private void OnVariantButtonClick(int variantSpeechId)
    {
        dialogueController.ChooseVariant(variantSpeechId);
    }
}
