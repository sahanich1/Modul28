using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimDisplay : MonoBehaviour
{
    [SerializeField]
    private GameObject targetInfo;

    private Camera mainCamera;

    private void Awake()
    {
        mainCamera = Camera.main;
    }

    public void Refresh(Actor target)
    {
        if (target != null)
        {
            RefreshInfoPosition(target);
            targetInfo.SetActive(true);
        }
        else
        {
            targetInfo.SetActive(false);
        }
    }

    private void RefreshInfoPosition(Actor target)
    {
        if (target == null)
        {
            return;
        }

        Vector3 currentPosition = target.GetInteractionDisplayPoint().position;
        targetInfo.transform.position = mainCamera.WorldToScreenPoint(currentPosition);
    }
}
