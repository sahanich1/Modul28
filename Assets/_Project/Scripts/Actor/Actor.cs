using System;
using UnityEngine;

public class Actor : MonoBehaviour
{
    [SerializeField]
    private Transform aimingStartPoint;
    [SerializeField]
    private ActorMovementBehaviour movementBehaviour;
    [SerializeField]
    private DialogueActor dialogueBehaviour;

    [Header("Interaction")]
    [SerializeField]
    private bool isInteractive = true;
    [SerializeField]
    private Transform interactionDisplayPoint;
    [SerializeField]
    private float maxDistanceForInteraction = 5;

    public float DistanceToCamera { get; private set; }

    public DialogueActor DialogueBehaviour => dialogueBehaviour;
    public float MaxDistanceForInteraction => maxDistanceForInteraction;
    public bool IsInteractive => isInteractive;

    public event Action<Collider> TriggerEntered;

    private void Awake()
    {
        DistanceToCamera = Vector3.Distance(transform.position, DialogueBehaviour.CameraPositionTransform.position);
    }

    private void OnTriggerEnter(Collider other)
    {
        TriggerEntered?.Invoke(other);    
    }

    public Transform GetInteractionDisplayPoint()
    {
        return interactionDisplayPoint;
    }

    public Vector3 GetAimingStartPoint()
    {
        return aimingStartPoint.transform.position;
    }

    public float GetAimingRadius()
    {
        return movementBehaviour.GetColliderRadius();
    }

    public void MoveBody(Vector3 move)
    {
        movementBehaviour.MoveBody(move);
    }

    public void Move(Vector3 move, bool isRunning)
    {
        movementBehaviour.Move(move, isRunning);
    }

    public void StopMoving()
    {
        movementBehaviour.StopMoving();
    }

    public void StartDialogue(DialogueController dialogueController)
    {      
        dialogueBehaviour.SubscribeToDialogueEvents(dialogueController);
    }

    public void StopDialogue(DialogueController dialogueController)
    {
        dialogueBehaviour.UnsubscribeFromDialogueEvents(dialogueController);
    }
}
