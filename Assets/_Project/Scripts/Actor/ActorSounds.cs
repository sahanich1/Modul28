﻿using UnityEngine;

public class ActorSounds : MonoBehaviour
{
    [SerializeField] 
    private AudioSource stepSound;

    public void PlayFootstep()
    {
        if (stepSound.isPlaying)
        {
            stepSound.Stop();
        }
        stepSound.pitch = Random.Range(0.7f, 1.3f);
        stepSound.Play();
    }
}
