using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorMovementBehaviour : MonoBehaviour
{
    [SerializeField]
    private CharacterController cc;
    [SerializeField]
    private Animator ac;
    [SerializeField]
    private float movingTurnSpeed = 360;
    [SerializeField]
    private float stationaryTurnSpeed = 180;
    [SerializeField]
    private float animSpeedMultiplier = 1f;

    private float turnAmount;
    private float forwardAmount;

    private int isWalkingHash;
    private int forwardFloatHash;
    private int turnFloatHash;

    private void Awake()
    {
        isWalkingHash = Animator.StringToHash(Constants.Animation.Parameter.WalkingBool);
        forwardFloatHash = Animator.StringToHash(Constants.Animation.Parameter.ForwardFloat);
        turnFloatHash = Animator.StringToHash(Constants.Animation.Parameter.TurnFloat);
    }

    public float GetColliderRadius()
    {
        return cc.radius;
    }

    public void MoveBody(Vector3 move)
    {
        cc.Move(move);
    }

    public void Move(Vector3 move, bool isRunning)
    {
        if (move.sqrMagnitude > 1f)
        {
            move.Normalize();
        }

        move = cc.transform.InverseTransformDirection(move);
        move.y = 0;

        turnAmount = Mathf.Atan2(move.x, move.z);
        forwardAmount = move.z;

        if (!isRunning)
        {
            forwardAmount *= 0.5f;
        }

        ApplyExtraTurnRotation();

        UpdateAnimator(move);
    }

    public void StopMoving()
    {
        UpdateAnimator(Vector3.zero);
    }

    private void UpdateAnimator(Vector3 move)
    {
        ac.SetFloat(forwardFloatHash, forwardAmount, 0.1f, Time.deltaTime);
        ac.SetFloat(turnFloatHash, turnAmount, 0.1f, Time.deltaTime);

        if (move.sqrMagnitude > 0)
        {
            ac.SetBool(isWalkingHash, true);
            ac.speed = animSpeedMultiplier;
        }
        else
        {
            ac.SetBool(isWalkingHash, false);
            ac.speed = 1;
        }
    }

    private void ApplyExtraTurnRotation()
    {
        float turnSpeed = Mathf.Lerp(stationaryTurnSpeed, movingTurnSpeed, forwardAmount);
        cc.transform.Rotate(0, turnAmount * turnSpeed * Time.deltaTime, 0);
    }
}
